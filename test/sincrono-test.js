'use strict'

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let util = require('util');
let assert = chai.assert;

chai.should();
chai.use(chaiHttp);

// objeto usuario 
let num = Math.floor((Math.random() * 100) + 1)
let user = {
    name: "user" + num,
    password: "A.b.123456",
    email: "user" + num + "@test.com"
}


before(done => {
    console.log('\n\n-----------------------\n--\n--  TEST\n--\n-------------------------');
    console.log('DATA TEST AUTOGENERADO')
    console.log("username:" + user.name);
    console.log("password:" + user.password);
    console.log("email:" + user.email);
    console.log('\n\n-----------------------\n--\n-- START TEST\n--\n-------------------------');
    done();
});

after(done => {
    console.log('\n\n-----------------------\n--\n-- END TEST\n--\n-------------------------');
    done();
});

// Prueba Unitaria
describe('#Prueba de Operaciones de Usuario.', () => {
    describe('##TEST Usuario', () => {
        it('Test registro "user" object:', done => {
            chai.request(server)
                .post('/authentication/register')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({username: user.name,
                       email: user.email,
                       password: user.password
                })
                .end(function (err, res) {
                    if (err) done(err);
                    assert.equal(res.statusCode,200);
                    assert.equal(res.body.success,true);
                    done();
                    
                    console.log('status code: %s, msj_server: %s', res.statusCode, res.body.message)
                });
          }).timeout(1000);

          it('Test login con user ' + user.name + ',email:' + user.email + ',password:' + user.password + ':', done => {
            chai.request(server)
                .post('/authentication/login')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({ username: user.name,
                        password: user.password    
                })
                .end(function (err, res) {
                    if (err) done(err);
                    assert.equal(res.statusCode,200);
                    assert.equal(res.body.success,true);
                    
                    user.token = res.body.token
                    done(); 
                    console.log('status code: %s, msj_server login: %s', res.statusCode, res.body.message)
                });
        }).timeout(1000);

       it('Test Get Perfil de usuario ' + user.name + ',email:' + user.email + ',password:' + user.password + ':', done => {
            chai.request(server)
                .get('/authentication/profile')
                .set('content-type', 'application/json')
                .set('authorization', user.token)
                .end(function (err, res) {
                    if (err) done(err);
                    assert.equal(res.statusCode,200);
                    assert.equal(res.body.success,true);
                    done();
                    
                    console.log('status code: %s, get perfil usuario: username: %s |email: %s', res.statusCode, res.body.user.username,res.body.user.email)
                });
        }).timeout(1000);
    })
})