/* ===================
   Import Node Modules
=================== */

const express = require('express');
const app = express();
const path = require('path');
const router = express.Router(); // crea un nuevo objeto router
const mongoose = require('mongoose'); // para Usar con MongoDB
mongoose.Promise = global.Promise;
const config = require('./config/database'); // configuracion de mongoose
const authentication = require('./routes/authentication')(router); // Ruta de autenticacion
const bodyParser = require('body-parser'); 
const cors = require('cors'); // CORS is a node.js package for providing a Connect/Express
//////////////////////////////////////////////////////////////////////////////////////////////


// Database Connection Mongo
mongoose.connect(config.uri, (err) => {
  if (err) {
    console.log('No pudo conectar a MongoDB: ', err);
  } else {
    console.log('Conectado a MongoDB: ' + config.db);
  }
});
//

//Middleware
//app.use(serveStatic(__dirname + '/tp-gcc/dist/'));
app.use(cors({ origin: 'http://localhost:3000' }));
app.use(bodyParser.urlencoded({ extended: false })); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(express.static(__dirname + '/tp-gcc/dist/')); //  directorio static del frontend
app.use('/authentication', authentication);
//



// Conexion de servidor a Angular  Index.html
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/tp-gcc/dist/index.html'));
});




// puerto 3000 para localhost
app.listen(process.env.PORT || 3000, function () {
  console.log('App listening on port 3000!');
});

module.exports = app