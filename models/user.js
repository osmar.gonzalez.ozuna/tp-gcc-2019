
const mongoose = require('mongoose');   // ODM
mongoose.Promise = global.Promise; // Configura Promesas Mongoose
const Schema = mongoose.Schema; // Import Schema from Mongoose
const bcrypt = require('bcrypt-nodejs'); //  bcrypt library for NodeJS

// validacion de email
let emailLengthChecker = (email) => {
  // verifica existencia del email
  if (!email) {
    return false; // return error
  } else {
    
    if (email.length < 5 || email.length > 30) {
      return false; // return error si no cumple la condicion
    } else {
      return true; // email es valido
    }
  }
};

// Validacion del formato del email
let validEmailChecker = (email) => {

  if (!email) {
    return false;
  } else {
    // Expresion regular para  validacion de email
    const regExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return regExp.test(email); // retorna true o false si es correcto
  }
};

// Array de Email validatos
const emailValidators = [
  {
    validator: emailLengthChecker,
    message: 'E-mail must be at least 5 characters but no more than 30'
  },
  {
    validator: validEmailChecker,
    message: 'Must be a valid e-mail'
  }
];

// Funcion que valida la longitud del username
let usernameLengthChecker = (username) => {
  // verifica si username existe
  if (!username) {
    return false; // error
  } else {

    if (username.length < 3 || username.length > 15) {
      return false; // no cumple los minimos requqrimientos
      return true; // username es valido.
    }
  }
};

// Funcion que validad el Formato de username
let validUsername = (username) => {
  // verifica si existe username
  if (!username) {
    return false; //error
  } else {
    // Expresion regular para testear el formato de username
    const regExp = new RegExp(/^[a-zA-Z0-9]+$/);
    return regExp.test(username);
  }
};

// Array de Username validators
const usernameValidators = [
  {
    validator: usernameLengthChecker,
    message: 'Username debe tener al menos 3 caracteres pero no más de 15.'
  },
  {
    validator: validUsername,
    message: 'Username no debe tener caracteres especiales.'
  }
];

// Funcion que Valida  la verificacion la longitud del password
let passwordLengthChecker = (password) => {
  // verifica si existe password
  if (!password) {
    return false; // return error
  } else {
    // verifica longitud del password
    if (password.length < 8 || password.length > 35) {
      return false; // Return error si no cumple la condicion
      return true; // password valido
    }
  }
};

// Funcion que validad el formato del password
let validPassword = (password) => {

  if (!password) {
    return false;
  } else {
    // Expresion regular para checkeo de password
    const regExp = new RegExp(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/);
    return regExp.test(password); // true o false si el formato del password es valido
  }
};

// Array de Password Validators
const passwordValidators = [

  {
    validator: passwordLengthChecker,
    message: 'Password debe tener al menos 8 caracteres pero no más de 35.'
  },
  {
    validator: validPassword,
    message: 'Debe tener al menos un carácter en mayúsculas, minúsculas, caracteres especiales y números.'
  }
];

// User Model
const userSchema = new Schema({
  email: { type: String, required: true, unique: true, lowercase: true, validate: emailValidators },
  username: { type: String, required: true, unique: true, lowercase: true, validate: usernameValidators },
  password: { type: String, required: true, validate: passwordValidators }
});

// Schema Middleware to Encrypt Password
userSchema.pre('save', function(next) {

  if (!this.isModified('password'))
    return next();

  // aplicar cifrado
  bcrypt.hash(this.password, null, null, (err, hash) => {
    if (err) return next(err); // ocurrio algun error?
    this.password = hash; // aplica cifrado al password
    next();
  });
});

// Método para comparar el password con el password cifrado al hacer login
userSchema.methods.comparePassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

// Export Module/Schema
module.exports = mongoose.model('User', userSchema);
