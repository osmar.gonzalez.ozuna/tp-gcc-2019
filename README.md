Esta aplicación fue creada con node.js, el API con el framework express.js en el backend y en el frontend Angular.

Para instalar localmente solo se debe clonar y ejecutar:
    **npm install**
    
Para correr la aplicacion localmente:
    **node server.js**

Las pruebas unitarias son realizadas automaticamente al hacer commit y push  utilizando la libreria mocha.js y chai.js, 
o se puede realizar manualmente con:
    **npm test**

Utiliza una conexion a Base de datos Mongo DB en la nube.
Se puede conectar a la base de datos usando algun cliente para mongo db como MongoDB Compass | user: admin , password: admin.
    **mongodb+srv://admin:admin@cluster0-46stw.mongodb.net/test**


Servidores:
    Desarrollo:
    URL: **https://app-gcc-dev.herokuapp.com/**
    
    Homologación:
    URL: **https://app-gcc-preprod.herokuapp.com/**
    
    Producción:
    URL: **https://app-gcc.herokuapp.com/**

**OBS: el deploy en el servidor de produccion es realizado solamente sobre la rama master y cuando haya un tag nuevo.**


**Proceso de integración continua**

Se realizan el test en todas las ramas.
Una vez realizado el test se hace deploy en la instancia de heroku correspondiente.

El Script, para cada rama (Produccion,Homologacion y develop), para hacer el deploy se instalan node.js y ruby
Se instala la gema "dpl" para luego utilizar el comando para hacer el deploy en heroku con las opciones
--provider=heroku --app=<nombre-app> --api-key=$HEROKU_API_KEY

El before_script lo unico que hace un put en /etc/apt/sources.list, debido a que al hacer apt-get update no encuentra el recurso en el sources.list
     
    

    


