const User = require('../models/user'); 
const jwt = require('jsonwebtoken'); 
const config = require('../config/database'); // configuracion de base de datos

module.exports = (router) => {
  
  router.post('/register', (req, res) => {
    // verifica si el email fue proveido
    if (!req.body.email) {
      res.json({ success: false, message: 'Debe proveer un email.' }); // return Error
    } else {
      // verifica si el username fue proveido
      if (!req.body.username) {
        res.json({ success: false, message: 'Debe proveer un username.' }); // return Error
      } else {
        // verifica si el password fue proveido
        if (!req.body.password) {
          res.json({ success: false, message: 'Debe proveer un password' }); 
        } else {
          // Crea un nuevo Objeto User y aplica user input
          let user = new User({
            email: req.body.email.toLowerCase(),
            username: req.body.username.toLowerCase(),
            password: req.body.password
          });
          // Guardar user en base de datos
          user.save((err) => {
            // verifica si ocurrio algun error
            if (err) {
              // verifica si error indica cuenta duplicada
              if (err.code === 11000) {
                res.json({ success: false, message: 'Username o email ya existen' }); 
              } else {
                    // verifica si error es validacion error
                    if (err.errors) {
                              // verifica si validacion de error es en el campo email
                              if (err.errors.email) {
                                res.json({ success: false, message: err.errors.email.message }); // return error
                              } else {
                                // Verifica si error es en el campo username 
                                if (err.errors.username) {
                                  res.json({ success: false, message: err.errors.username.message }); // return error
                                } else {
                                  // Verifica si error es en el password
                                  if (err.errors.password) {
                                    res.json({ success: false, message: err.errors.password.message }); // return error
                                  } else {
                                    res.json({ success: false, message: err }); // devuelve cualquier otro error no cubierto
                                  }
                                }
                              }
                    } else {
                      res.json({ success: false, message: 'No se pudo guardar usuario. Error: ', err }); // return error si no esta relacionado con la validación
                    }
              }
            } else {
              res.json({ success: true, message: 'Cuenta Registrada con exito' }); // return success true
            }
          });
        }
      }
    }
  });

  /* //////////////////////////////////////
     Route to: comprueba si el email del usuario esta disponible para el registro
  ///////////////////////////////////////// */

  router.get('/checkEmail/:email', (req, res) => {
    // Verifica si email fue proveido en parametros
    if (!req.params.email) {
      res.json({ success: false, message: 'Email no fue proveido' }); // return error
    } else {
      // busqueda por user's email en base de datos;
      User.findOne({ email: req.params.email }, (err, user) => {
        if (err) {
          res.json({ success: false, message: err }); // return conecction error
        } else {
          // Check if user's e-mail is taken
          if (user) {
            res.json({ success: false, message: 'E-mail is already taken' }); // Return as taken e-mail
          } else {
            res.json({ success: true, message: 'E-mail is available' }); // Return as available e-mail
          }
        }
      });
    }
  });

  /* /////////////////////////////////////////////////
     Route to:  Comprueba si el username del usuario esta disponible para el registro
  /////////////////////////////////////////////////// */
  
  router.get('/checkUsername/:username', (req, res) => {
    // verifica si username fue proveido en los parametros
    if (!req.params.username) {
      res.json({ success: false, message: 'Username no fue proveido.' }); // return error succes false
    } else {
      // buscar por username en base de datos
      User.findOne({ username: req.params.username }, (err, user) => {
        // verifica si ocurrio un error de conexion
        if (err) {
          res.json({ success: false, message: err }); // return connection error
        } else {
          // verifica si el username fue encontrado
          if (user) {
            res.json({ success: false, message: 'Username ya esta en uso.' }); 
          } else {
            res.json({ success: true, message: 'Username disponible' }); 
          }
        }
      });
    }
  });

  /* ////////////////
  ROUTE Login
  /////////////// */
  router.post('/login', (req, res) => {
    // verifica si username fue proveido
    if (!req.body.username) {
      res.json({ success: false, message: 'Username no proveido.' }); // return error
    } else {
      // verifica si password fue proveido
      if (!req.body.password) {
        res.json({ success: false, message: 'Password no proveido.' }); // return error
      } else {
        // Verifica en Base de datos si existe username
        User.findOne({ username: req.body.username.toLowerCase() }, (err, user) => {
          // si ocurrio un error
          if (err) {
            res.json({ success: false, message: err }); // return error
          } else {
            // Verifica si el username fue encontrado
            if (!user) {
              res.json({ success: false, message: 'Username no fue encontrado.' }); // return error
            } else {
              const validPassword = user.comparePassword(req.body.password); // compara el password proveido por el que esta en BD.
              // verifica si matcheo el password
              if (!validPassword) {
                res.json({ success: false, message: 'Password invalido' }); // return error
              } else {
                // Crea el token para el cliente
                const token = jwt.sign({ userId: user._id }, config.secret, { expiresIn: '24h' }); 
                res.json({ success: true, message: 'Exitoso!', token: token, user: { username: user.username } }); // Return success y el  token al frontend angular
              }
            }
          }
        });
      }
    }
  });

  /* //////////////////////////////////
  MIDDLEWARE - Se utiliza para tomar el token del usuario del header
  //////////////////////////////// */
  router.use((req, res, next) => {
    const token = req.headers['authorization']; // Crea token encontrado en headers
    // verifica si encontro el tocken en el header
    if (!token) {
      res.json({ success: false, message: 'token no proveido.' }); // return error
    } else {
      // verifica si es valido el token
      jwt.verify(token, config.secret, (err, decoded) => {
        // verifica si error es invalido o expirado
        if (err) {
          res.json({ success: false, message: 'Token invalido: ' + err }); // return error por validadcion de token
        } else {
          req.decoded = decoded; //Crea una variable global para usar en cualquier solicitud despues
          next(); // Exit middleware
        }
      });
    }
  });

  /* ////////////////////
     Route to: obtiene el perfil del usuario
  /////////////////////// */
  router.get('/profile', (req, res) => {
    // busca un usuario en BD.
    User.findOne({ _id: req.decoded.userId }).select('username email').exec((err, user) => {
      // si ocurrio un error de conexion
      if (err) {
        res.json({ success: false, message: err }); // return error
      } else {
        // Comprueba si el usuario fue encontrado en base de datos
        if (!user) {
          res.json({ success: false, message: 'Usuario no encontrado.' }); // return error , No encontro usuario
        } else {
          res.json({ success: true, user: user }); // return success, retorna user profile al frontend
        }
      }
    });
  });

  return router; // return objeto router al main server.js
}
